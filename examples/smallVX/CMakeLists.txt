#----------------------------------------------------------------------------
# Setup the project
cmake_minimum_required(VERSION 2.6 FATAL_ERROR)
project(smallVX)


#----------------------------------------------------------------------------
# Find Geant4 package, activating all available UI and Vis drivers by default
# You can set WITH_GEANT4_UIVIS to OFF via the command line or ccmake/cmake-gui
# to build a batch mode only executable
#
option(WITH_GEANT4_UIVIS "Build example with Geant4 UI and Vis drivers" ON)
if(WITH_GEANT4_UIVIS)
  find_package(Geant4 REQUIRED ui_all vis_all)
else()
  find_package(Geant4 REQUIRED)
endif()
FIND_PACKAGE( Boost 1.52 REQUIRED )

set(CMAKE_MODULE_PATH
    ${PROJECT_SOURCE_DIR}/modules
    ${CMAKE_MODULE_PATH})

include(FindMongoDB)

#set(BOOST_INCLUDE_DIR "C:/Temp/boost_1_55_0")
#set(MONGOCXX_INCLUDE_DIR "C:/Temp/mongocxx/install/include")
#set(MONGOCXX_LINK_DIR "C:/Temp/mongocxx/install/lib")
set(NPPLAN_LINK_DIR "C:/Temp/geantProjects/NPLibrary_build2/Release")
set(NPPLAN_INCLUDE_DIR "C:/Temp/geantProjects/NPLibrary/include")

#----------------------------------------------------------------------------
# Setup Geant4 include directories and compile definitions
#
include(${Geant4_USE_FILE})

#----------------------------------------------------------------------------
# Locate sources and headers for this project
#
include_directories(${PROJECT_SOURCE_DIR}/include 
                    ${Geant4_INCLUDE_DIR}
#                    ${BOOST_INCLUDE_DIR}
                    ${Boost_INCLUDE_DIR}
#                    ${MONGOCXX_INCLUDE_DIR}
                    ${MongoDB_INCLUDE_DIR}
                    ${NPPLAN_INCLUDE_DIR}
)
file(GLOB sources ${PROJECT_SOURCE_DIR}/src/*.cc)
file(GLOB headers ${PROJECT_SOURCE_DIR}/include/*.hh)


message(STATUS ${Boost_LIBRARIES})
#----------------------------------------------------------------------------
# Add the executable, and link it to the Geant4 libraries
#
add_executable(smallVX smallVX.cc ${sources} ${headers})
target_link_libraries(smallVX 
 ${Geant4_LIBRARIES} 
 ${Boost_LIBRARIES}
 ${MongoDB_LIBRARIES}
 ${NPPLAN_LINK_DIR}
 ${NPPLAN_LINK_DIR}/NPLibrary.lib
)
link_directories (${Boost_LIBRARY_DIRS})
link_directories (${MongoDB_LIBRARIES})
link_directories (${NPPLAN_LINK_DIR})
#LINK_DIRECTORIES(${BOOST_INCLUDE_DIR}/stage/lib)
#LINK_DIRECTORIES(${MONGOCXX_LINK_DIR})

#----------------------------------------------------------------------------
# Copy all scripts to the build directory, i.e. the directory in which we
# build Hadr03. This is so that we can run the executable directly because it
# relies on these scripts being in the current working directory.
#

#----------------------------------------------------------------------------
# Install the executable to 'bin' directory under CMAKE_INSTALL_PREFIX
#
#install(TARGETS testG4DB DESTINATION bin)

