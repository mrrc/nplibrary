#ifndef NPRun_1
#define NPRun_1
#include "G4Event.hh"
#include "G4Run.hh"
#include "globals.hh"
#include <map>
#include "DetectorConstruction.hh"

#include "NPLayerData.hh"

class DetectorConstruction;

class Run : public G4Run
{
public:
  Run(DetectorConstruction*);
  virtual ~Run();

  void addProcessCounter(G4String, G4double);
  void addF2Analog(G4double, G4double, G4String);
  void addPipeTargetDep(G4double);

  void addDetData(G4String, G4double, G4double);

  virtual void Merge(const G4Run*);
  void EndOfRun();

  void addDataFromSD(G4int, G4double, G4double, G4double);
  
private:
  DetectorConstruction* fDetector;
  std::map<G4String, G4int> processMap;
  std::map<G4String, G4double> processDeps;

  std::map<G4String, G4double> mlF2Analog;

  std::map<G4String, runDataLayer> runDataDets;

  std::map<G4int, expFullData> fullVXDepData;

  G4double ene, ene2;
  G4int ncc, nccNZ;
};

#endif