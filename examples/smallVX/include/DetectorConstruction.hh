#ifndef DetectorConstruction_H
#define DetectorConstruction_H 1

#include "G4VUserDetectorConstruction.hh"
#include <vector>
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4UImessenger.hh"


//class G4LogicalVolume;
//class G4ThreeVector;
//class G4Material;
class G4UIdirectory;
//class G4UIcmdWith3VectorAndUnit;
//class G4UIcmdWithoutParameter; 
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithADouble;
class G4UIcmdWithABool;
class DetectorSD;
class DetectorConstruction;
class DetectorMessenger;


class DetectorConstruction: public G4VUserDetectorConstruction
{                 
  public:
    DetectorConstruction();
    ~DetectorConstruction();

    G4VPhysicalVolume* Construct();
    void ConstructSDandField();
    void constructMaterials();

    G4String filterParamString;

  private:
    G4bool activeBoxLVisSet;
    G4LogicalVolume *ActiveBoxLV;

    void createPhantom(G4LogicalVolume *, G4ThreeVector, G4RotationMatrix*);
    

};


#endif

