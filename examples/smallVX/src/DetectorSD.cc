#include "DetectorSD.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4RunManager.hh"
#include "RunAction.hh"
#include "G4ParticleTypes.hh"
#include "G4IonTable.hh"
#include "G4GenericIon.hh"
#include "NPlayerData.hh"
#include "G4Analyser.hh"
#include "g4root.hh"
#include "G4RunManager.hh"
#include "G4Run.hh"
#include "Run.hh"
#include <map>

DetectorSD::DetectorSD(G4String name, G4int lX, G4int lY, G4int lZ): G4VSensitiveDetector(name), ownName(name), fnX(lY), fnY(lY), fnZ(lZ)
{

}
DetectorSD::~DetectorSD() {
}

void DetectorSD::Initialize(G4HCofThisEvent*) {
  //G4cout<<static_cast<G4Run*>(G4RunManager::GetRunManager()->GetNonConstCurrentRun())->GetNumberOfEvent()<<G4endl;
  curEventNo = static_cast<G4Run*>(G4RunManager::GetRunManager()->GetNonConstCurrentRun())->GetNumberOfEvent();
  curEventParsed = false;
}

//#define USE_VOXEL_METHOD 1

G4bool DetectorSD::ProcessHits(G4Step* step, G4TouchableHistory* aHist) {
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();


  G4int iz = step->GetTrack()->GetTouchable()->GetCopyNumber(0);
  G4int iy = step->GetTrack()->GetTouchable()->GetCopyNumber(1);
  G4int ix = step->GetTrack()->GetTouchable()->GetCopyNumber(2);

  G4int copyID = iz + fnZ*iy + fnY*fnZ*ix;

  static_cast<Run*>(G4RunManager::GetRunManager()->GetNonConstCurrentRun())->addDataFromSD(copyID, step->GetTotalEnergyDeposit(), step->GetStepLength(), -1);

  vxMain *vxData = vxMain::Instance();

  vxData->addData(copyID, step->GetTotalEnergyDeposit(), step->GetStepLength(), step->GetTrack()->GetKineticEnergy(), step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessSubType());

  //G4cout<<"DSD: "<<ix<<" "<<iy<<" "<<iz<<" "<<copyID<<" "<<step->GetTotalEnergyDeposit() / keV<<G4endl;
  
  return true;
}

void DetectorSD::EndOfEvent(G4HCofThisEvent*) {
  curEventParsed = false;
}
