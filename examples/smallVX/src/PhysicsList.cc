//#include "WLSExtraPhysics.hh"
#include "G4VUserPhysicsList.hh"
#include "PhysicsList.hh"
#include "G4LossTableManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"

// EM
#include "G4EmLivermorePhysics.hh"
#include "G4EmPenelopePhysics.hh"
#include "G4EmStandardPhysics.hh"
#include "G4EmStandardPhysics_option1.hh"
#include "G4EmStandardPhysics_option2.hh"
#include "G4EmStandardPhysics_option3.hh"
#include "G4EmStandardPhysics_option4.hh"
#include "G4EmStandardPhysicsWVI.hh"
#include "G4EmStandardPhysicsSS.hh"
#include "G4EmLowEPPhysics.hh"

// hadron
#include "G4HadronPhysicsQGSP_BIC.hh"
//#include "G4HadronPhysicsQGSP_BIC_AllHP.hh"
#include "G4HadronPhysicsQGSP_BIC_HP.hh"
#include "G4HadronPhysicsINCLXX.hh"
#include "G4HadronPhysicsFTFP_BERT.hh"
#include "G4EmExtraPhysics.hh"      
#include "G4HadronElasticPhysics.hh"
#include "G4HadronElasticPhysicsHP.hh"
#include "G4StoppingPhysics.hh"
#include "G4IonBinaryCascadePhysics.hh"
#include "G4NeutronTrackingCut.hh"
#include "G4IonElasticPhysics.hh"
#include "G4IonQMDPhysics.hh"
#include "G4DecayPhysics.hh"
#include "G4IonPhysics.hh"
#include "G4IonINCLXXPhysics.hh"

// constructors
#include "G4BosonConstructor.hh"
#include "G4LeptonConstructor.hh"
#include "G4MesonConstructor.hh"
#include "G4BosonConstructor.hh"
#include "G4BaryonConstructor.hh"
#include "G4IonConstructor.hh"
#include "G4ShortLivedConstructor.hh"

//#include "myPhysics.hh"
#include "NPparamReader.hh"
#include "NPEmPhysics.hh"
#include "NPHadrPhysics.hh"
#include "NPPhysicsReader.hh"


myPhysicsList::myPhysicsList():G4VModularPhysicsList() {
  //G4LossTableManager::Instance();
  defaultCutValue = 25.*micrometer;
  myParamReader &paramReader = myParamReader::GetInstance();

  G4String emPhysics = paramReader.getOtherParamAsString("--em", "opt4");
  G4cout<<"emPhysics at PhysicsList: "<<emPhysics<<G4endl;
  if ("opt4" == emPhysics) {
    emPhysicsList = new G4EmStandardPhysics_option4();
  } else if ("opt3" == emPhysics) {
    emPhysicsList = new G4EmStandardPhysics_option3();
  } else if ("opt2" == emPhysics) {
    emPhysicsList = new G4EmStandardPhysics_option2();
  } else if ("opt1" == emPhysics) {
    emPhysicsList = new G4EmStandardPhysics_option1();
  } else if ("opt0" == emPhysics) {
    emPhysicsList = new G4EmStandardPhysics();
  } else if ("liver" == emPhysics) {
    emPhysicsList = new G4EmLivermorePhysics();
  } else if ("penel" == emPhysics) {
    emPhysicsList = new G4EmPenelopePhysics();
  } else if ("WVI" == emPhysics) {
    emPhysicsList = new G4EmStandardPhysicsWVI();
  } else if ("SS" == emPhysics) {
    emPhysicsList = new G4EmStandardPhysicsSS();
  } else {
    //emPhysicsList = new G4EmStandardPhysics_option4();
    emPhysicsList = new NPEmPhysics(1);
  }
  //emPhysicsList = new G4EmStandardPhysics();

  // EM Physics
  //this->RegisterPhysics( new G4EmStandardPhysics() );

  // Synchroton Radiation & GN Physics
  //hadronPhys.push_back( new G4EmExtraPhysics() );

  // Decays
  //hadronPhys.push_back( new G4DecayPhysics() );

   // Hadron Elastic scattering
  //hadronPhys.push_back( new G4HadronElasticPhysics() );

   // Hadron Physics
  //hadronPhys.push_back(  new G4HadronPhysicsQGSP_BIC());

  // Stopping Physics
  //hadronPhys.push_back( new G4StoppingPhysics() );

  // Ion Physics
  //hadronPhys.push_back( new G4IonPhysics());
  G4String useHadrPhysics = paramReader.getOtherParamAsString("--useHadr", "0");
  if ("1" == useHadrPhysics) {
    hadronPhys.push_back( new NPHadrPhysics());
  } else if ("2" == useHadrPhysics) {
    hadronPhys.push_back( new G4HadronPhysicsQGSP_BIC_HP(1));
    hadronPhys.push_back( new G4HadronElasticPhysicsHP(1));
    hadronPhys.push_back(new G4EmExtraPhysics(1));
    hadronPhys.push_back(new G4IonElasticPhysics(1));  
    hadronPhys.push_back(new  G4IonPhysics(1));
    hadronPhys.push_back( new G4StoppingPhysics(1));
    //hadronPhys.push_back( new G4IonBinaryCascadePhysics(1));
    //hadronPhys.push_back( new G4NeutronTrackingCut(1));
  }
  // Neutron tracking cut
  //hadronPhys.push_back( new G4NeutronTrackingCut());
  //hadronPhys.push_back(new G4IonElasticPhysics());
  //hadronPhys.push_back(new G4IonQMDPhysics());
  //emPhysicsList = new G4EmLivermorePhysics();
  //hadronPhys.push_back( new G4HadronPhysicsQGSP_BIC());
  //hadronPhys.push_back( new G4HadronPhysicsINCLXX());
  //hadronPhys.push_back( new G4EmExtraPhysics());
  //hadronPhys.push_back( new G4HadronElasticPhysics());
  //hadronPhys.push_back( new G4StoppingPhysics());
  //hadronPhys.push_back( new NPHadrPhysics());
  //hadronPhys.push_back( new G4IonBinaryCascadePhysics());
  //hadronPhys.push_back( new G4NeutronTrackingCut());
  //hadronPhys.push_back(new G4IonINCLXXPhysics());
  //hadronPhys.push_back( new myPhysic());
  /*hadronPhys.push_back(new G4EmExtraPhysics());
  hadronPhys.push_back(new G4IonBinaryCascadePhysics());
  dronPhys.push_back(new G4StoppingPhysics());
  hadronPhys.push_back(new G4HadronPhysicsQGSP_BIC());
  hadronPhys.push_back(new G4HadronElasticPhysics());*/

  G4RunManager::GetRunManager() -> PhysicsHasBeenModified();
}

myPhysicsList::~myPhysicsList() {
}

void myPhysicsList::ConstructParticle() {
  G4BosonConstructor  pBosonConstructor;
  pBosonConstructor.ConstructParticle();

  G4LeptonConstructor pLeptonConstructor;
  pLeptonConstructor.ConstructParticle();

  G4MesonConstructor pMesonConstructor;
  pMesonConstructor.ConstructParticle();

  G4BaryonConstructor pBaryonConstructor;
  pBaryonConstructor.ConstructParticle();

  G4IonConstructor pIonConstructor;
  pIonConstructor.ConstructParticle();

  G4ShortLivedConstructor pShortLivedConstructor;
  pShortLivedConstructor.ConstructParticle();  

  emPhysicsList->ConstructParticle();
  for(size_t i=0; i<hadronPhys.size(); i++) {
    hadronPhys[i] -> ConstructParticle();
  }  
  //G4VUserPhysicsList::ConstructParticle();
}

void myPhysicsList::ConstructProcess() {
  AddTransportation();

  emPhysicsList->ConstructProcess();

  for(size_t i=0; i<hadronPhys.size(); i++) {
    hadronPhys[i] -> ConstructProcess();
  }

  //G4VUserPhysicsList::ConstructProcess();

  return;
}

void myPhysicsList::SetCuts() {
  SetCutValue(25.*micrometer, "gamma");
  SetCutValue(25.*micrometer, "e-");
  SetCutValue(25.*micrometer, "e+");
  /*SetCutValue(0.7*mm, "gamma");
  SetCutValue(0.7*mm, "e+");
  SetCutValue(0.7*mm, "e-");*/
  //SetCutValue(5*micrometer, "gamma");
  //SetCutValue(5*micrometer, "e+");
  //SetCutValue(5*micrometer, "e-");
  SetCutValue(0, "proton");
}