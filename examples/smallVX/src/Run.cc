#include "Run.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "globals.hh"
#include "G4RunManager.hh"
#include "DetectorConstruction.hh"
#include "NPLayerData.hh"

#include "g4root.hh"

Run::Run(DetectorConstruction* fdet)
 : G4Run(), fDetector(fdet)
{
  ene = 0.0;
  ene2 = 0.0;
  ncc = 0;
  //runDataDets
}

Run::~Run()
{;}

void Run::addPipeTargetDep(G4double _ene) {
  ene += _ene;
  ene2 += _ene*ene;
  ncc += 1;
  if (_ene > 0.0) {
    nccNZ += 1;
  }
}

void Run::addProcessCounter(G4String proc, G4double dose) {
  if (0 == processMap.count(proc)) {
    processMap.insert(std::pair<G4String, G4int>(proc, 1));
    processDeps.insert(std::pair<G4String, G4double>(proc, dose));
  } else {
    processMap.find(proc)->second += 1;
    processDeps.find(proc)->second += dose;
  }
}

void Run::addF2Analog(G4double dE, G4double dX, G4String origin) {
  if (0 == mlF2Analog.count(origin)) {
    mlF2Analog.insert(std::pair<G4String, G4double>(origin, dE*dX));
  } else {
    mlF2Analog.find(origin)->second += dE*dX;
  }
}

void Run::addDataFromSD(G4int voxID, G4double dE, G4double dX, G4double lt) {
  runDataLayer tmp;
  tmp.Nullify();
  tmp.Update(dE, dX, lt);
  //expFullData tmpFull;
  if (fullVXDepData.count(voxID) == 0) {
    expFullData tmpFull;
    tmpFull.depData = tmp;
    fullVXDepData.insert(std::pair<G4int, expFullData>(voxID, tmpFull));
  } else {
    fullVXDepData.find(voxID)->second.depData += tmp;
  }
}

void Run::EndOfRun() {
  G4cout<<"Process map at active layer (with non-zero deposit):" <<G4endl;
  for (std::map<G4String, G4int>::const_iterator itp = processMap.begin(); itp != processMap.end(); ++itp) {
    G4cout<<std::fixed<<std::setw(20)<<itp->first<<" "<<std::setw(7)<<itp->second<<" acts. - "<<std::scientific<<std::setw(15)<<std::setprecision(10)<<processDeps.find(itp->first)->second / MeV<<" MeV"<<G4endl;
  }

  // G4RunManager::RMType rmtype = G4RunManager::GetRunManager()->GetRunManagerType()

  vxMain *vxmain = vxMain::Instance();
  vxmain->setVoxelData(5, 6, 20, G4ThreeVector(2, 3, 4));
  G4cout<<"VOXEL TEST BEGIN"<<G4endl;
  G4cout<<vxmain->getNumFromXYZCoord(2, 3, 9)<<G4endl;
  G4cout<< vxmain->getXCoordFromNum(vxmain->getNumFromXYZCoord(2, 3, 9))<<G4endl;
  G4cout<< vxmain->getYCoordFromNum(vxmain->getNumFromXYZCoord(2, 3, 9))<<G4endl;
  G4cout<< vxmain->getZCoordFromNum(vxmain->getNumFromXYZCoord(2, 3, 9))<<G4endl;
  G4cout<<"VOXEL TEST END"<<G4endl;

  G4cout<<"Average F2-like data: "<<G4endl;
  for (std::map<G4String, G4double>::const_iterator itp = mlF2Analog.begin(); itp != mlF2Analog.end(); ++itp) {
    G4double f2an = 1.0;
    G4cout<<
      std::setw(14)<<
      itp->first<<" "
      <<itp->second
      <<std::scientific<<std::setw(15)<<std::setprecision(10)
      <<" (MeV*cm), "
      <<std::scientific<<std::setw(15)<<std::setprecision(10)
      <<(itp->second / f2an) / (MeV/cm2) <<" MeV/cm2, " 
      <<std::scientific<<std::setw(15)<<std::setprecision(10)
      << (itp->second / f2an) / (joule/cm2) <<" joule/cm2 "<<G4endl;
  }

  //G4cout<<G4endl;

            // ax, ay, az -> x,y,z limits ?
            //int zN = int(ix/(ax*ay));
            //int yN = int((ix-(int(ix/(ax*ay)))*ax*ay)/ax);
            //int xN = ix-int((ix-(int(ix/(ax*ay)))*ax*ay)/ax)*ax - (int(ix/(ax*ay)))*ax*ay;

  for (std::map<G4int, expFullData>::const_iterator itp = fullVXDepData.begin(); itp != fullVXDepData.end(); ++itp) {
    G4cout<<itp->first
      <<" "
      <<std::scientific<<std::setprecision(8)<<std::setw(10)<<itp->second.depData.depEnergy / MeV
      <<" "
      <<std::scientific<<std::setprecision(8)<<std::setw(10)<<itp->second.depData.depEnergy2 / (MeV*MeV)
      <<" "
      <<std::scientific<<std::setprecision(8)<<std::setw(10)<<itp->second.depData.nEvents
      <<" "
      <<std::scientific<<std::setprecision(8)<<std::setw(10)<<itp->second.depData.nEventsNonZero
      <<" "
      <<std::scientific<<std::setprecision(8)<<std::setw(10)<<(itp->second.depData.letBase / itp->second.depData.depEnergy) / (keV/um)
      <<" "
      <<std::scientific<<std::setprecision(8)<<std::setw(10)<<itp->second.depData.letBase2 / ((MeV*MeV/mm)/(MeV*MeV/mm))
      <<" "
      <<std::scientific<<std::setprecision(8)<<std::setw(10)<<itp->second.depData.cellFlux / (1/cm2)
      <<" "
      <<std::scientific<<std::setprecision(8)<<std::setw(10)<<itp->second.depData.cellFlux2/ ((1/cm2)*(1/cm2))
      <<" "
      <<G4endl;
  }

  vxMain *vxData = vxMain::Instance();
  vxData->printData();
}
  
void Run::addDetData(G4String name, G4double dE, G4double dX) {
  //fDetector->activeCellLayerLV->GetSolid()->GetCubicVolume();
  //fDetector->activeCellLayerLV->GetMass();
  runDataLayer nn = runDataLayer();
  // @todo: rework this !!!!
  //nn.Update(dE, dX, dX/fDetector->activeCellLayerLV->GetSolid()->GetCubicVolume());
  //nn.Update(dE, dX, dX/fDetector->detMainLayerLV->GetSolid()->GetCubicVolume());
  nn.Update(dE, dX, -1);
  //nn.Update(dE, dX, dX/(fDetector->detMainLayerLV->GetSolid()->GetCubicVolume() + fDetector->detEndLayerLV->GetSolid()->GetCubicVolume()));
  if (0 == runDataDets.count(name)) {
    runDataDets.insert(std::pair<G4String, runDataLayer>(name, nn));
  } else {
    runDataDets.find(name)->second += nn;
  }
}

void Run::Merge(const G4Run* run) {
  const Run* localRun = static_cast<const Run*>(run);
  for (std::map<G4String, G4int>::const_iterator itp = localRun->processMap.begin(); itp != localRun->processMap.end(); ++itp) {
    if (0 == processMap.count(itp->first)) {
      processMap.find(itp->first)->second = itp->second;
    } else {
      processMap.find(itp->first)->second += itp->second;
    }
  }

  for (std::map<G4String, runDataLayer>::const_iterator itp = localRun->runDataDets.begin(); itp != localRun->runDataDets.end(); ++itp) {
    if(0 == runDataDets.count(itp->first)) {
      runDataDets.insert(std::pair<G4String, runDataLayer>(itp->first, itp->second));
    } else {
      runDataDets.find(itp->first)->second += itp->second;
    }
  }

  ene += localRun->ene;
  ene2 += localRun->ene2;
  ncc += localRun->ncc;

  G4Run::Merge(run); 
}