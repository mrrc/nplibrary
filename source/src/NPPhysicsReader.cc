#include "NPPhysicsReader.hh"

#include <boost/config/compiler/visualc.hpp>
//#include <boost/config/compiler/gcc.hpp>  
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <ios>
#include <string>
#include "G4ParticleDefinition.hh"
#include "G4HadronicProcess.hh"
#include "G4VIntraNuclearTransportModel.hh"
#include "G4HadronElastic.hh"
#include "G4ProcessManager.hh"
#include "globals.hh"

namespace NPPhysicsReader {
  void setModel(std::string physicsFilePath, G4ParticleDefinition* particle, G4ProcessManager* pManager, NPHadrPhysics* NPPhys) {
    //G4ProcessManager* pManager = particle->GetProcessManager();
    G4String pname = particle->GetParticleName();

    std::map<G4String, G4HadronicProcess*> pMap;
    
    boost::property_tree::ptree doc;
    boost::property_tree::read_json(physicsFilePath, doc);
    BOOST_FOREACH (boost::property_tree::ptree::value_type& interactionPair, doc.get_child(pname.c_str())) {
      G4HadronicProcess *process1;
          BOOST_FOREACH (boost::property_tree::ptree::value_type& intDataPair, interactionPair.second) {
            if ("name" == intDataPair.first) {
              process1 = (G4HadronicProcess*)NPPhys->pFactory.construct(intDataPair.second.get_value<std::string>());
              pManager->AddDiscreteProcess(process1);
              pMap.insert(std::pair<G4String, G4HadronicProcess*>(interactionPair.first, process1));
            }
          }
    }

    // need second cycle because ordering at json object not lexical
    BOOST_FOREACH (boost::property_tree::ptree::value_type& interactionPair, doc.get_child(pname.c_str())) {
          BOOST_FOREACH (boost::property_tree::ptree::value_type& intDataPair, interactionPair.second) {
            if ("z_models" == intDataPair.first) {
              BOOST_FOREACH (boost::property_tree::ptree::value_type& modelsDataPair, intDataPair.second) {
                G4HadronElastic *mlEl;
                G4VIntraNuclearTransportModel *mlInel;
                BOOST_FOREACH (boost::property_tree::ptree::value_type& smDataPair, modelsDataPair.second) {
                  if (interactionPair.first == "elastic" && smDataPair.first == "name") {
                    mlEl = (G4HadronElastic*)NPPhys->elModelsFactory.construct(smDataPair.second.get_value<std::string>());
                    pMap.find(interactionPair.first)->second->RegisterMe(mlEl);
                  } else if (interactionPair.first == "inelastic" && smDataPair.first == "name") {
                    mlInel = (G4VIntraNuclearTransportModel*)NPPhys->inelModelsFactory.construct(smDataPair.second.get_value<std::string>());
                    pMap.find(interactionPair.first)->second->RegisterMe(mlInel);
                  }
                  // @todo: add capture/fission/nKiller
                  else {
                    if (interactionPair.first == "elastic" && smDataPair.first == "z_lowEnergy") {
                      mlEl->SetMinEnergy(  boost::lexical_cast<G4double>( smDataPair.second.get_value<std::string>() ));
                    }
                    if (interactionPair.first == "elastic" && smDataPair.first == "z_highEnergy") {
                      mlEl->SetMaxEnergy(  boost::lexical_cast<G4double>( smDataPair.second.get_value<std::string>() ));
                    }
                    if (interactionPair.first == "elastic" && smDataPair.first == "z_lowEnergy") {
                      mlInel->SetMinEnergy(  boost::lexical_cast<G4double>( smDataPair.second.get_value<std::string>() ));
                    }
                    if (interactionPair.first == "elastic" && smDataPair.first == "z_highEnergy") {
                      mlInel->SetMaxEnergy(  boost::lexical_cast<G4double>( smDataPair.second.get_value<std::string>() ));
                    }
                  }
                }
              }
            }
          }

    }
    // @todo: read cross sections
  }

  std::vector<std::string> getListParticles(std::string physicsFilePath) {
    boost::property_tree::ptree doc;
    boost::property_tree::read_json(physicsFilePath, doc);
    std::vector<std::string> ret;
    BOOST_FOREACH (boost::property_tree::ptree::value_type& partsPair, doc) {
      ret.push_back(partsPair.first);
    }
    return ret;
  }

};