#include "NPLayerData.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <boost/archive/binary_oarchive.hpp> 
#include <boost/archive/binary_iarchive.hpp> 
#include <boost/serialization/map.hpp> 
#include <boost/serialization/string.hpp> 
#include <boost/serialization/list.hpp> 
#include <boost/serialization/vector.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/serialization/access.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/string.hpp>
#include <boost/lexical_cast.hpp>

#include "NPExtra.hh"

#ifdef G4MULTITHREADED
#include "G4Threading.hh"
#include "G4AutoLock.hh"
#endif

using namespace boost;
using namespace boost::archive;
using namespace boost::serialization;

detLayer::detLayer() { 
  Nullify();
}


G4int processNameToNum(G4String procName) {
  if ("" == procName)
    return -1;
  if ("emsc" == procName)  // electron multiple scattering
    return 1;  
  if ("eIoni" == procName) // electron ionization via livermore
    return 2;
  if ("ec" == procName) //electron coloumb scattering
    return 3;
  if ("eBrem" == procName) // electron bremsstrahlung (all models)
    return 4;
  return -1;
}

runDataLayer::runDataLayer() {
  Nullify();
}

void runDataLayer::Nullify() {
  depEnergy = 0.0;
  depEnergy2 = 0.0;
  nEvents = 0.0;
  nEventsNonZero = 0.0;
  cellFlux = 0.0;
  cellFlux2 = 0.0;
  letBase = 0.0;
  letBase2 = 0.0;
}

//runDataLayer::runDataLayer(G4double dE, G4int nE, G4int nZ, G4double cf) {
//  depEnergy = dE;
//  depEnergy2 = dE*dE;
//  nEvents = nE;
//  nEventsNonZero = nZ;
//  cellFlux = cf;
//  cellFlux2 = cf*cf;
//  //letBase = dE*dE/nE;
//  //letBase2 = (dE*dE/nE)*(dE*dE/nE);
//}
//
//runDataLayer::runDataLayer(G4double dE, G4int nE, G4int nZ, G4double cf, G4double let) {
//  depEnergy = dE;
//  depEnergy2 = dE*dE;
//  nEvents = nE;
//  nEventsNonZero = nZ;
//  cellFlux = cf;
//  cellFlux2 = cf*cf;
//  letBase = let;
//  letBase2 = let*let;
//}
runDataLayer::runDataLayer(G4double _dE, G4double _dE2, G4int _nE, G4int _nZ, G4double _let, G4double _let2, G4double _cf, G4double _cf2):
  depEnergy(_dE), depEnergy2(_dE2),
  nEvents(_nE), nEventsNonZero(_nZ),
  letBase(_let), letBase2(_let2),
  cellFlux(_cf), cellFlux2(_cf2)
{
}

runDataLayer::runDataLayer(G4double _dE, G4double _let, G4double _cf): depEnergy(_dE), letBase(_let), cellFlux(_cf), nEvents(1) {
  depEnergy2 = _dE*_dE;
  letBase2 = _let*_let;
  cellFlux2 = _cf*_cf;
  if (_dE > 0.0)
    nEventsNonZero = 1;
}

void runDataLayer::Update(G4double dE, G4double dX, G4double cf) {
  depEnergy += dE;
  depEnergy2 += dE*dE;
  if (dX > 0.0) {
    letBase += dE*dE/dX;
    letBase2 += (dE*dE/dX)*(dE*dE/dX);
  }
  nEvents++;
  if (depEnergy != 0) {
    nEventsNonZero++;
  }
  cellFlux += cf;
  cellFlux2 += cf*cf;
  // dX unused now
}

runDataLayer runDataLayer::operator+ (const runDataLayer &other) {
  runDataLayer nn = runDataLayer();
  nn.depEnergy = depEnergy + other.depEnergy;
  nn.depEnergy2 = depEnergy2 + other.depEnergy2;
  nn.letBase = letBase + other.letBase;
  nn.letBase2 = letBase2 + other.letBase2;
  nn.cellFlux = cellFlux + other.cellFlux;
  nn.cellFlux2 = cellFlux2 + other.cellFlux2;
  nn.nEvents = nEvents + other.nEvents;
  nn.nEventsNonZero = nEventsNonZero + other.nEventsNonZero;
  return nn;
}
runDataLayer& runDataLayer::operator+= (const runDataLayer &other) {
  depEnergy += other.depEnergy;
  depEnergy2 += other.depEnergy2;
  letBase += other.letBase;
  letBase2 += other.letBase2;
  cellFlux += other.cellFlux;
  cellFlux2 += other.cellFlux2;
  nEvents += other.nEvents;
  nEventsNonZero += other.nEventsNonZero;
  return *this;
}

detLayer::detLayer(G4double en, G4double en2, G4double _do, G4double _do2, G4int nE, G4double cf, G4double cf2): depEnergy(en),
  depEnergy2(en2), dose(_do), dose2(_do2), nEvents(nE), cellFlux(cf), cellFlux2(cf2)
{ 
}

bool detLayer::Update(G4double ene, G4double _do, G4int ne, G4double cf) {
  depEnergy += ene;
  depEnergy2 += ene*ene;
  dose += _do;
  dose2 += _do*_do;
  nEvents += ne;
  cellFlux += cf;
  cellFlux2 += cf*cf;
  return true;
}

bool detLayer::Update(G4double ene, G4double _do, G4double cf) {
  depEnergy += ene;
  depEnergy2 += ene*ene;
  dose += _do;
  dose2 += _do*_do;
  nEvents++;
  cellFlux += cf;
  cellFlux2 += cf*cf;
  return true;
}

void detLayer::Nullify() {
  depEnergy = 0.0 * MeV;
  depEnergy2 = 0.0;
  dose = 0.0 * MeV;
  dose2 = 0.0;
  cellFlux = 0.0 * (1/cm2);
  cellFlux2 = 0.0;
  nEvents = 0;
}

letLayer::letLayer() {
  totalLet = 0.0;
  dTotalLet = 0.0;
  totalLet2 = 0.0;
  dTotalLet2 = 0.0;
}

letLayer::letLayer(G4double _t, G4double _dt) {
  totalLet = _t;
  dTotalLet = _dt;
}

void letLayer::Nullify() {
  totalLet = 0.0;
  totalLet2 = 0.0;
  dTotalLet = 0.0;
  dTotalLet2 = 0.0;
}

bool letLayer::Update(G4double _t, G4double _dt) {
  totalLet += _t;
  totalLet2 += _t*_t;
  dTotalLet += _dt;
  dTotalLet2 += _dt * _dt;
  return true;
}

letLayer letLayer::operator+ (const letLayer &other) {
  letLayer nn = letLayer(totalLet + other.totalLet, dTotalLet + other.dTotalLet);
  nn.totalLet2 = totalLet2 + other.totalLet2;
  nn.dTotalLet2 = dTotalLet2 + other.dTotalLet2;
  return nn;
}
letLayer& letLayer::operator+= (const letLayer &other) {
  totalLet += other.totalLet;
  totalLet2 += other.totalLet2;
  dTotalLet += other.dTotalLet;
  dTotalLet2 += other.dTotalLet2;
  return *this;
}

detLayerStepWeighted::detLayerStepWeighted() {
  Nullify();
}

detLayerStepWeighted::detLayerStepWeighted(G4double k, G4double k2, G4double e, G4double e2, G4double s, G4double s2, G4int n): eKins(k),
  eKins2(k2), eDep(e), eDep2(e2), sLength(s), sLength2(s2), nEvents(n)
{
}

void detLayerStepWeighted::Nullify() {
  eKins = 0.0;
  eKins2 = 0.0;
  eDep = 0.0;
  eDep2 = 0.0;
  sLength = 0.0;
  sLength2 = 0.0;
  nEvents = 0;
}

bool detLayerStepWeighted::Update(G4double k, G4double d, G4double s) {
  eKins += k*s;
  eKins2 += k*s*k*s;
  eKinsNw += k;
  eKinsNw2 += k*k;
  //eDep += d*s;
  //eDep2 += d*d*s*s;
  eDep += d;
  eDep2 += d*d;
  sLength += s;
  sLength2 += s*s;
  nEvents += 1;
  return true;
}

detLayerStepWeighted detLayerStepWeighted::operator+ (const detLayerStepWeighted &other) {
  G4double eKin = eKins + other.eKins;
  G4double eKin2 = eKins2 + other.eKins2;
  G4double ed = eDep + other.eDep;
  G4double ed2 = eDep2 + other.eDep2;
  G4double s = sLength + other.sLength;
  G4double s2 = sLength2 + other.sLength2;
  G4int ne = nEvents + other.nEvents;
  return detLayerStepWeighted(eKin, eKin2, ed, ed2, s, s2, ne);
}

detLayerStepWeighted& detLayerStepWeighted::operator+=(const detLayerStepWeighted &other) {
  eKins += other.eKins;
  eKins2 += other.eKins2;
  eDep += other.eDep;
  eDep2 += other.eDep2;
  sLength += other.sLength;
  sLength2 += other.sLength2;
  nEvents += other.nEvents;
  return *this;
}

resLayer::resLayer(): _errorState(false) {
  Nullify();
}

resLayer::resLayer(G4double ene, G4double ene2, G4double _do, G4double _do2, G4int nE, G4double cf, G4double cf2): _errorState(false),
  depEnergy(ene), depEnergy2(ene2), dose(_do), dose2(_do2), nEvents(nE), cellFlux(cf), cellFlux2(cf2)
{

}
resLayer::resLayer(detLayer data): _errorState(false) {
  depEnergy = data.depEnergy;
  depEnergy2 = data.depEnergy2;
  dose = data.dose;
  dose2 = data.dose2;
  nEvents = data.nEvents;
  cellFlux = data.cellFlux;
  cellFlux2 = data.cellFlux2;
}

resLayer::resLayer(G4double ene, G4double ene2, G4double eneErr, G4double _do, G4double _do2, G4double _doErr, G4int nE, G4double cf, G4double cf2, G4double cfErr): _errorState(true),
  depEnergy(ene), depEnergy2(ene2), dose(_do), dose2(_do2), nEvents(nE), cellFlux(cf), depEnergyError(eneErr), doseError(_doErr), cellFlux2(cf2), cellFluxError(cfErr)
{

}

void resLayer::Nullify() {
  depEnergy = 0.0 * MeV;
  depEnergy2 = 0.0;
  depEnergyError = 0.0 * MeV;
  dose = 0.0 * MeV;
  dose2 = 0.0;
  doseError = 0.0 * MeV;
  nEvents = 0;
  cellFlux = 0.0 * (1/cm2);
  cellFlux2 = 0.0;
  cellFluxError = 0.0 * (1/cm2);
}

resLayer resLayer::operator+ (const resLayer &other) {
  _errorState = false;
  G4double ene = depEnergy + other.depEnergy;
  G4double ene2 = depEnergy2 + other.depEnergy2;
  G4double _do = dose + other.dose;
  G4double _do2 = dose2 + other.dose2;
  G4double nE = nEvents + other.nEvents;
  G4double cf = cellFlux + other.cellFlux;
  G4double cf2 = cellFlux2 + other.cellFlux2;
  return resLayer(ene, ene2, _do, _do2, nE, cf, cf2);
}

resLayer resLayer::operator+ (const detLayer &other) {
  _errorState = false;
  G4double ene = depEnergy + other.depEnergy;
  G4double ene2 = depEnergy2 + other.depEnergy2;
  G4double _do = dose + other.dose;
  G4double _do2 = dose2 + other.dose2;
  G4int nE = nEvents + other.nEvents;
  G4double cf = cellFlux + other.cellFlux;
  G4double cf2 = cellFlux2 + other.cellFlux2;
  return resLayer(ene, ene2, _do, _do2, nE, cf, cf2);
}

resLayer &resLayer::operator+= (const resLayer &other) {
  _errorState = false;
  depEnergy += other.depEnergy;
  depEnergy2 += other.depEnergy2;
  dose += other.dose;
  dose2 += other.dose2;
  nEvents += other.nEvents;
  cellFlux += other.cellFlux;
  cellFlux2 += other.cellFlux2;
  return *this;
}

resLayer &resLayer::operator+= (const detLayer &other) {
  _errorState = false;
  depEnergy += other.depEnergy;
  depEnergy2 += other.depEnergy2;
  dose += other.dose;
  dose2 += other.dose2;
  nEvents += other.nEvents;
  cellFlux += other.cellFlux;
  cellFlux2 += other.cellFlux2;
  return *this;
}

void resLayer::calculateError() {
  G4double v, vDo, c;
  v = nEvents*depEnergy2 - depEnergy*depEnergy;
  vDo = nEvents*dose2 - dose*dose;
  c = nEvents*cellFlux2 - cellFlux*cellFlux;
  if (nEvents>1) {
    depEnergyError = 1.0*std::sqrt(v/(nEvents-1));
    doseError = 1.0*std::sqrt(vDo/(nEvents-1));
    cellFluxError = 1.0*std::sqrt(c/(nEvents-1));
  }
  _errorState = true;
}


bool filterType::allowed(G4double kineticEnergy, G4double let, G4int procType) {
  if (usecase == filterType::npF_unused)
    return true;
  if (usecase == filterType::npF_gtKinetic && kineticEnergy > criteria.kineticCriteria) 
    return true;
  if (usecase == filterType::npF_lwKinetic && kineticEnergy < criteria.kineticCriteria) 
    return true;
  if (usecase == filterType::npF_gtDeDx && let > criteria.deDxCriteria) 
    return true;
  if (usecase == filterType::npF_lwDeDx && let < criteria.deDxCriteria) 
    return true;
  if (usecase == filterType::npF_procType && procType == criteria.processTypeCriteria)
    return true;
  return false;
}

G4double filterType::getActiveCriteria() {
  if (usecase == filterType::npF_unused)
    return -1;
  if (usecase == filterType::npF_gtKinetic) 
    return criteria.kineticCriteria;
  if (usecase == filterType::npF_lwKinetic) 
    return criteria.kineticCriteria;
  if (usecase == filterType::npF_gtDeDx) 
    return criteria.deDxCriteria;
  if (usecase == filterType::npF_lwDeDx) 
    return criteria.deDxCriteria;
  if (usecase == filterType::npF_procType)
    return (G4double)criteria.processTypeCriteria;
  return -1;
}

G4String filterType::getActiveCriteriaString() {
  if (usecase == filterType::npF_unused)
    return G4String("All");
  if (usecase == filterType::npF_gtKinetic) 
    return G4String("Greater than kinetic");
  if (usecase == filterType::npF_lwKinetic) 
    return G4String("Lower than kinetic");
  if (usecase == filterType::npF_gtDeDx) 
    return G4String("Greater than LET");
  if (usecase == filterType::npF_lwDeDx) 
    return G4String("Lower than LET");
  if (usecase == filterType::npF_procType)
    return G4String("Process subtype");
  return G4String("Unknown");
}

void filterType::setCriteria(G4double _criteria) {
  if (usecase == filterType::npF_unused)
    return;
  if (usecase == filterType::npF_gtKinetic || usecase == filterType::npF_lwKinetic) 
    criteria.kineticCriteria = _criteria;
  if (usecase == filterType::npF_gtDeDx || usecase == filterType::npF_lwDeDx) 
    criteria.deDxCriteria = _criteria;
  if (usecase == filterType::npF_procType)
    criteria.processTypeCriteria = (int)_criteria;
}

#ifdef G4MULTITHREADED
namespace {
  G4Mutex vxMainMergeMutex = G4MUTEX_INITIALIZER;
}
vxMain* vxMain::vxMasterInstance = 0;
G4ThreadLocal vxMain* vxMain::vxInstance = 0;
#else
vxMain* vxMain::vxInstance = 0;
#endif

#ifdef G4MULTITHREADED
vxMain* vxMain::Instance()
{
  if ( vxInstance == 0 ) {
    G4bool isMaster = ! G4Threading::IsWorkerThread();
    vxInstance = new vxMain(isMaster);
    vxInstance->Init();
  }
  
  return vxInstance;
}    
#else
vxMain* vxMain::Instance()
{
  if ( vxInstance == 0 ) {
    vxInstance = new vxMain();
    vxInstance->Init();
  }
  return vxInstance;
}
#endif

vxMain::~vxMain() {
}

#ifdef G4MULTITHREADED
vxMain::vxMain(bool isMaster) {
  if ( isMaster ) vxMasterInstance = this;
  vxInstance = this;
}
#else
vxMain::vxMain() {
}
//vxMain::vxMain() {
//  filterUsedCounter = 0;
//  filterType ft;
//  ft.usecase = filterType::npF_unused;
//  _addFilterToMap(ft);
////  vxInstance = this;
////  
//}
#endif

void vxMain::Init() {
  // @todo: disable init for the 2nd time
  isMetaSet = false;
  filterUsedCounter = 0;
  filterType ft;
  ft.usecase = filterType::npF_unused;
  _addFilterToMap(ft);
}

void vxMain::_addFilterToMap(filterType ft) {
  if (fdData.count(filterUsedCounter) != 0) {
    G4cout<<"NPLibrary::Fatal > Wrong filter number"<<G4endl;
    return;
  }
  //fdData.insert(std::pair<G4int, filterType>(filterUsedCounter, ft));
  _addFilterToMap(ft, filterUsedCounter);
}
void vxMain::_addFilterToMap(filterType ft, int cnt) {
  // don't use this function directly, only for loading from binary dump
  fdData.insert(std::pair<G4int, filterType>(cnt, ft));
  runDataLayer tL;
  tL.Nullify();
  std::map<G4int, runDataLayer> tM;
  tM.insert(std::pair<G4int, runDataLayer>(-1, tL));
  _rdAll.insert(std::pair<G4int, voxeledRunDataLayer>(cnt, tM));
}

void vxMain::setVoxelData(G4int _ax, G4int _ay, G4int _az, G4ThreeVector _vSize) {
  if (isMetaSet)
    return;  // probably throw exception ?
  metaQX = _ax;
  metaQY = _ay;
  metaQZ = _az;
  metaVxSize = _vSize;
  isMetaSet = true;
}

//void vxMain::addFilter(G4double gtKinetic, G4double lwKinetic, G4double gtDeDx, G4double lwDeDx, G4int procType) {
void vxMain::addFilter(G4double criteria, filterType::usecasetype _usecase) {
  filterUsedCounter++;
  filterType ft;
  ft.usecase = _usecase;
  ft.setCriteria(criteria);
  
  _addFilterToMap(ft);
}
void vxMain::addData(G4int voxNum, G4double dE, G4double dX, G4double eKin, G4int processType) {
  G4double dEdX;
  if (dE >= 0.0 && dX >= 0.0) {
    dEdX = dE/dX;
  }
  for(std::map<G4int, filterType>::iterator it = fdData.begin(); it != fdData.end(); ++it) {
    if (it->second.allowed(eKin, dEdX, processType)) {
      _addDataToRD(it->first, voxNum, dE, dX);
      if (std::find(affectedVoxelNums.begin(), affectedVoxelNums.end(), voxNum) == affectedVoxelNums.end()) {
        affectedVoxelNums.push_back(voxNum);
      }
    }
  }
}

void vxMain::_addDataToRD(G4int rdNum, G4int voxNum, G4double dE, G4double dX) {
  // refactored 10.06.2015 @todo: check merge
  if (_rdAll.find(rdNum)->second.count(voxNum) == 0) {
      runDataLayer curVX;
      curVX.Update(dE, dX, 1/(dX*dX));
      _rdAll.find(rdNum)->second.insert(std::pair<G4int, runDataLayer>(voxNum, curVX));
  } else {
    _rdAll.find(rdNum)->second.find(voxNum)->second.Update(dE, dX, 1/(dX*dX));
  }
  _rdAll.find(rdNum)->second.find(-1)->second.Update(dE, dX, 1/(dX*dX));
}

// simple methods for conversion flat array to 3D-shaped array index
// @todo: check x,y,z -> z,y,x
G4int vxMain::getZCoordFromNum(G4int bn) {
  if (!isMetaSet)
    return n_NOT_FOUND;
  return bn-int((bn-(int(bn/(metaQZ*metaQY)))*metaQZ*metaQY)/metaQZ)*metaQZ - (int(bn/(metaQZ*metaQY)))*metaQZ*metaQY;
}
G4int vxMain::getYCoordFromNum(G4int bn) {
  if (!isMetaSet)
    return n_NOT_FOUND;
  return int((bn-(int(bn/(metaQY*metaQZ)))*metaQY*metaQZ)/metaQZ);
}
G4int vxMain::getXCoordFromNum(G4int bn) {
  if (!isMetaSet)
    return n_NOT_FOUND;
  return int(bn/(metaQY*metaQZ));
}
G4int vxMain::getNumFromXYZCoord(G4int ix, G4int iy, G4int iz) {
  if (!isMetaSet)
    return n_NOT_FOUND;
  G4int l = iz + metaQZ*iy + metaQY*metaQZ*ix;
  return (l < metaQZ*metaQY*metaQX) ? l : n_WRONG_DATA;
}

void vxMain::printData() {
  // @todo: implement accurate table
  G4cout<<"Stats with "<<(filterUsedCounter+1)<<" active filters"<<G4endl;
  G4cout<<"Affected voxels: "<<affectedVoxelNums.size()<<"/"<<metaQX*metaQY*metaQZ<<G4endl;
  G4cout<<"--------------------------------------------------"<<G4endl;
  for(int i = 0; i <= filterUsedCounter; i++) {
    G4cout<<"Filter "<<i<<": "<<fdData.find(i)->second.getActiveCriteriaString()<<" == "<<fdData.find(i)->second.getActiveCriteria()<<G4endl; // @todo: handle unit
    // @todo: refactor this
    std::map<G4int, runDataLayer> curP;
    curP = _rdAll.find(i)->second;
    for(std::map<G4int, runDataLayer>::iterator it = curP.begin(); it != curP.end(); ++it) {
      G4double let = 0.0;
      G4double letError, l1e, l2e;
      if (it->second.depEnergy > 0.0) {
        let = it->second.letBase / it->second.depEnergy;
        // @todo: check nEvents vs nEventsNonZero
        l1e = getAbsError(it->second.letBase, it->second.letBase2, it->second.nEventsNonZero);
        l2e = getAbsError(it->second.depEnergy, it->second.depEnergy2, it->second.nEvents);
        letError = (l1e > l2e) ? (l1e / l2e) : (l2e/l1e);
      } else {
        let = -1;
        letError = -1;
      }
      G4cout<<
        std::setprecision(8)<<std::setw(16)
        <<std::fixed<<it->first
        <<" "
        <<std::scientific<<it->second.depEnergy / MeV
        <<" "
        //<<std::scientific<<it->second.depEnergy2
        <<getRelErrorString(it->second.depEnergy, getAbsError(it->second.depEnergy, it->second.depEnergy2, it->second.nEvents))
        <<" "
        <<std::fixed<<it->second.nEvents
        <<" "
        <<std::fixed<<it->second.nEventsNonZero
        <<" "
        <<std::scientific<<let / (keV/um)
        <<" "
        //<<std::scientific<<it->second.letBase2
        <<getRelErrorString(let, letError)
        <<" "
        <<std::scientific<<it->second.cellFlux
        <<" "
        //<<std::scientific<<it->second.cellFlux2
        <<getRelErrorString(it->second.cellFlux, getAbsError(it->second.cellFlux, it->second.cellFlux2, it->second.nEvents))
        <<G4endl;
    }
    
  }
}

void vxMain::finalize() {
#ifdef G4MULTITHREADED
  if ( ! G4Threading::IsWorkerThread() )  {
    // master
  } else {
    G4AutoLock lMut(&vxMainMergeMutex);
    vxMasterInstance->merge(-1, _rdAll);
    lMut.unlock();
  }
#endif
}

void vxMain::dump(G4String fName) {
  /**
   * function dumps results to the files {fName} (simulation table) and _filters_{fName} (data for used filters).
   * Don't call this method from thread
   */
  // @todo: implement gzip
  // @todo: implement storing to single file
  //std::ofstream dStream;
  //dStream.open(fName.c_str(), std::ios::binary);
  //boost::iostreams::filtering_stream<boost::iostreams::output> fStream;
  //fStream.push(boost::iostreams::gzip_compressor());
  //fStream.push(dStream);
  std::ofstream fStream;
  fStream.open(fName.c_str(), std::ios::binary);
  boost::archive::binary_oarchive oa(fStream); 
  oa<<*this;
  //oa.save();
  //dStream.close();
  fStream.close();
  G4cout<<fName<<" saved"<<G4endl;
  fName = G4String("_filters_"+fName);
  fStream.open(fName.c_str(), std::ios::binary);
  boost::archive::binary_oarchive oaF(fStream); 
  for( int i = 1; i <= filterUsedCounter; i++) {
    //oaF<<fdData.find(i);
    oaF<<fdData.find(i)->second.usecase;
    std::stringstream ssBuf;
    if (fdData.find(i)->second.usecase == filterType::npF_gtKinetic || fdData.find(i)->second.usecase == filterType::npF_lwKinetic) {
      ssBuf<<fdData.find(i)->second.getActiveCriteria() / MeV;
    } else if (fdData.find(i)->second.usecase == filterType::npF_gtDeDx || fdData.find(i)->second.usecase == filterType::npF_lwDeDx) {
      ssBuf<<fdData.find(i)->second.getActiveCriteria() / (keV/um);
    } else {
      ssBuf<<fdData.find(i)->second.getActiveCriteria();
    }
    std::string ssBufTmp(ssBuf.str());
    oaF<<ssBufTmp; // this hack is needed for gcc, msvc/intel on win can handle ssBuf output directly
  }
  fStream.close();
  G4cout<<fName<<" saved"<<G4endl;
}

void vxMain::readDump(G4String fName) {
  /**
   * Function reads data from {fName} and _filters_{fName} to own instance
   * @todo: check thread safety
   */
  std::ifstream dStream;
  G4cout<<"Attempt to read "<<fName<<G4endl;
  dStream.open(fName.c_str(), std::ios::binary);
  if (dStream.is_open()) {
    boost::archive::binary_iarchive ia(dStream); 
    ia>>*this;
  }
  dStream.close();

  fName = G4String("_filters_"+fName);
  dStream.open(fName.c_str(), std::ios::binary);
  int cntFilters = 0;
  if (dStream.is_open()) {
    boost::archive::binary_iarchive ia(dStream);
    for(int i = 1; i<=filterUsedCounter; i++) {
      filterType ft;
      std::string critString;
      ia>>ft.usecase;
      ia>>critString;
      if (ft.usecase == filterType::npF_gtKinetic || ft.usecase == filterType::npF_lwKinetic) {
        ft.setCriteria(boost::lexical_cast<G4double>(critString)*MeV);
      } else if (ft.usecase == filterType::npF_gtDeDx || ft.usecase == filterType::npF_lwDeDx) {
        ft.setCriteria(boost::lexical_cast<G4double>(critString)*(keV/um));
      } else if (ft.usecase == filterType::npF_procType) {
        ft.setCriteria(boost::lexical_cast<G4int>(critString));
      } else {
      }
      cntFilters++;
      _addFilterToMap(ft, cntFilters);
    }
  }
}

G4double vxMain::getAbsError(G4double val, G4double val2, G4int nv) {
  /**
   * calculate dispersion of value (val)
   */
  G4double v;
  v = nv*val2 - val*val;
  if (nv > 1) {
    return 1.0*std::sqrt(v/(nv-1));
  }
  return val;
}

G4String vxMain::getRelErrorString(G4double val, G4double err) {
  std::ostringstream ss;
  if (val == 0.0 || err == 0.0) {
    ss<<std::fixed<<std::setw(6)<<100;
  } else {
    ss<<std::fixed<<std::setw(6)<<std::setprecision(4)<< (err / val) * 100;
  }
  //ss<<std::fixed<<std::setw(6)<<std::setprecision(4)<<err;
  return G4String(ss.str());
}

#ifdef G4MULTITHREADED
void vxMain::merge(G4int rdNum, std::map<G4int, voxeledRunDataLayer> rdTarget) {
  // refactored 10.06.2015 @todo: check implementation
  if (-1 == rdNum) {
    for(std::map<G4int, voxeledRunDataLayer>::iterator itFilter = rdTarget.begin(); itFilter != rdTarget.end(); ++itFilter) {
      for(std::map<G4int, runDataLayer>::iterator it = itFilter->second.begin(); it != itFilter->second.end(); ++it) {
        if ( _rdAll.find(itFilter->first)->second.count(it->first) == 0) {
          _rdAll.find(itFilter->first)->second.insert(std::pair<G4int, runDataLayer>(it->first, it->second));
        } else {
          _rdAll.find(itFilter->first)->second.find(it->first)->second += it->second;
        }
      }
    }
  }
}
#endif