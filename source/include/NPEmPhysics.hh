#ifndef NPEmPhysics_h
#define NPEmPhysics_h 1

#include "G4VPhysicsConstructor.hh"
#include "globals.hh"

class NPEmPhysics : public G4VPhysicsConstructor
{
public:
  NPEmPhysics(G4int ver = 1);

  virtual ~NPEmPhysics();

  virtual void ConstructParticle();
  virtual void ConstructProcess();

private:
  G4int  verbose;
};


#endif