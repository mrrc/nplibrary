#ifndef NPHadrPhysics_h
#define NPHadrPhysics_h 1

#include "globals.hh"
#include "G4VPhysicsConstructor.hh"

#include <boost/function/function0.hpp>

template <class T> void* constructor() { return (void*)new T(); }

struct factory
{
   typedef void*(*constructor_t)();
   typedef std::map<std::string, constructor_t> map_type;
   map_type m_classes;

   template <class T>
   void register_class(std::string const& n)
   { m_classes.insert(std::make_pair(n, &constructor<T>)); }

   void* construct(std::string const& n)
   {
      map_type::iterator i = m_classes.find(n);
      if (i == m_classes.end()) return 0; // or throw or whatever you want
      return i->second();
   }
};


class NPHadrPhysics : public G4VPhysicsConstructor
{
  public:
    NPHadrPhysics(const G4String& name="NPHadr");
   ~NPHadrPhysics();

    void callProcessFactory();
    factory pFactory;
    void callElasticModelsFactory();
    factory elModelsFactory;
    void callInelasticModelsFactory();
    factory inelModelsFactory;

  public:
    virtual void ConstructParticle();
    virtual void ConstructProcess();

};
#endif