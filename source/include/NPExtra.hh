#ifndef NPExtra_h
#define NPExtra_h

#include "globals.hh"
#include <vector>
#include <map>

class mDBSingleton {
private:
   static mDBSingleton* instance;
   mDBSingleton() { }
   std::map<G4int, G4double> ltT;
   std::map<G4String, G4int> string2IntMetaData;
public:
   static mDBSingleton* GetInstance();
   void addlt(G4int a, G4double b);
   G4double getlt(G4int a);

   void addMetaData(G4String, G4int);
   G4int getS2IMeta(G4String);
};

enum ERR_CODES {
  n_NOT_FOUND,
  n_OTHER_TYPE,
  n_WRONG_DATA
};

namespace NPExtra {
  void __GetMachineName(char* machineName);
}

#endif