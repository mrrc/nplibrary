#ifndef paramReader_h
#define paramReader_h
#include "G4String.hh"
#include <vector>
#include <map>

#include "boost/lexical_cast.hpp"

class myParamReader {
  public:
    /*static myParamReader* GetInstance() {
      if(!mySingletonObject){
          mySingletonObject=new myParamReader;
      }
      return mySingletonObject;
    }*/
    static myParamReader& GetInstance() {
      static myParamReader instance;
      return instance;
    }
    void setVerbose(int v) {verboseLevel = v;};
    void setInput(int, char**);
    G4String getCmd() {return cmd;};
    G4String getRandomEngineString() {return randomEngine;};
    G4bool isUseTimeSeed() {return isTimeSeed;};
    long long getUserSeed() {return (!isTimeSeed) ? userSeed : (long long)time(NULL);};
    //HepRandomEngine* getRandomEngineObject();
    G4String getOtherParamAsString(G4String pKey);
    G4String getOtherParamAsString(G4String pKey, G4String);
    G4bool isUseUI() {return useUI;};
    G4bool isUseBasePhysics() {return isBasePhysics;};
    G4int getPhysicsModifierValue();
    G4int getPhysicsModifierValue(G4bool oldStyle);
    //std::pair<G4String, G4String> returnOtherParamAsPair(G4String pKey);
  private:
    //static myParamReader *mySingletonObject;
    myParamReader() {};
    myParamReader(myParamReader const&);
    void operator=(myParamReader const&);

    int verboseLevel;
    bool readComplete;  /**< flag of completion reading and parsing */
    void preInit();
    void postInit();

    G4String cmd; /**< filename to parse with /control/execute after all done */
    G4String randomEngine; /**< String name of random engine */
    G4bool useUI; /**< Try to use GUI classes, necessary for windows or default vis.mac cannot be parsed */
    G4bool isBasePhysics; /**< Internal flag is the physics to use in project is one of base physics (QGSP_BIC, FTFP_BERT, etc) */
    G4int basePhysicsCase;
    G4int myPhysicsModifier;
    G4bool isTimeSeed;
    long long userSeed;

    std::map<G4String, G4String> otherParams; /**< Map of key->value for all other params */
};


class NPParamReader2 {
  public:
    static NPParamReader2* Instance();
    G4int getSomething();

private:
   static NPParamReader2* NPParamInstance;
   NPParamReader2() { };
};

//myParamReader* myParamReader::mySingletonObject=NULL;
#endif